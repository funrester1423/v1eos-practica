#include <iostream>
#include <string>

std::string translate(std::string line, std::string argument){
	std::string input = line;
	char letter;
	for(unsigned int i=0; input[i] != '\0'; i++){
		letter=input[i];
		if(letter >= 'a' && letter <= 'z'){
			letter=letter + 3;
			if(letter > 'z'){
				letter = letter - 'z' + 'a' - 1;
			}
			input[i] = letter;
		}else if(letter >= 'A' && letter <= 'Z'){
			letter = letter + 3;
			if(letter > 'Z'){
				letter = letter - 'Z' + 'A' - 1;
			}
			input[i] = letter;
		}
	}
	std::string result = input; // implementeer dit
	return result;
}

int main(int argc, char *argv[])
{ std::string line;

  if(argc != 2)
  { std::cerr << "Deze functie heeft exact 1 argument nodig" << std::endl;
    return -1; }

  while(std::getline(std::cin, line))
  { std::cout << translate(line, argv[1]) << std::endl; } 

  return 0; }
